import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class example01 {

	public static void main(String[] args) {
		System.out.println("Hello World");
		System.setProperty("webdriver.chrome.driver","/Users/abhishekbansal/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();
		
		
		
		driver.get("http://www.google.com");
		driver.findElement(By.name("q")).sendKeys("Sacred Game Episodes8");
		driver.findElement(By.name("btnK")).click();

		
		String baseUrl = "http://demo.guru99.com/test/newtours/";
		String expectedTitle = "Welcome: Mercury Tours";
		String actualTitle = "";
		
		// launch Fire fox and direct it to the Base URL
		driver.get(baseUrl);
		
		// get the actual value of the title
		actualTitle = driver.getTitle();
		
		/*
		 * compare the actual title of the page with the expected one and print
		 * the result as "Passed" or "Failed"
		 */
		if (actualTitle.contentEquals(expectedTitle)){
		    System.out.println("Test Passed!");
		} else {
		    System.out.println("Test Failed");
		    }
		   
		    //close Chrome
		    //driver.close();

	}

}
