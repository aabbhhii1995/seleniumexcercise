import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class InputBoxTest {
	
	
	
	
	WebDriver driver ; 
	final String URL = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";
	final String DRIVER_PATH = "/Users/abhishekbansal/Downloads/chromedriver";
	@BeforeEach
	void setUp() throws Exception {
		
		// setting up the chrome driver
		System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
		
		driver = new ChromeDriver();
		
		driver.get(URL);

		
		
	}

	@AfterEach
	void tearDown() throws Exception {
		
		Thread.sleep(2000);
		driver.close();
		
	}

	@Test
	void testSingleInputField() throws InterruptedException {
		
		
				//1. find the text box (id = user-message)
				//2. type "hello world" in text box (sendKeys("..."))
				//3. find the button (cssSelector = form#get-input button)
				//4. click on the button (.click())
				//5. find the output message thing (id = display)
				//6. check the output message (expected result = actual result)
				         // (.getText())
				
				//1.
				WebElement textBox = driver.findElement(By.id("user-message"));
				//2.
				textBox.sendKeys("HELLO WORLD");
				//3.
				WebElement button = driver.findElement(By.cssSelector("form#get-input button"));
				//4.
				button.click();
				
				WebElement outputSpan = driver.findElement(By.id("display"));
				String outputMessage = outputSpan.getText();

					assertEquals("HELLO WORLD",outputMessage);
						
				// at the end show the output and close the browser 
				
				
	}
	
	@Test
	void testTwoInputFields() throws InterruptedException {
		
		
		
		WebElement textBox1 = driver.findElement(By.id("sum1"));
		textBox1.sendKeys("50");

		WebElement textBox2 = driver.findElement(By.id("sum2"));
		textBox2.sendKeys("70");
		
		WebElement button = driver.findElement(By.cssSelector("form#gettotal button"));
		button.click();	
		
		WebElement outputSpan = driver.findElement(By.id("displayvalue"));
		String outputMessage = outputSpan.getText();		//actual output
		
		assertEquals("120", outputMessage);
		
		// ---------------------------
		// sleep + close the browser
		
		
	}

}
